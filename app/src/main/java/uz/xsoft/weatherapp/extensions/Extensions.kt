package uz.xsoft.weatherapp.extensions

import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.R
import androidx.navigation.fragment.NavHostFragment

fun Fragment.findNavController(): NavController =
    NavHostFragment.findNavController(this)

fun Fragment.findNavController(id: Int): NavController =
    Navigation.findNavController(activity!!, id)

fun Fragment.findNavController(id: Fragment): NavController =
    NavHostFragment.findNavController(id)

fun Fragment.startAction(id: Int) {
    findNavController().navigate(id)
}


fun Fragment.startAction(id: NavDirections) {
    findNavController().navigate(id)
}