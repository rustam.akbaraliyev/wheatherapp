package uz.xsoft.weatherapp.utils

import androidx.recyclerview.widget.DiffUtil
import uz.xsoft.weatherapp.data.models.Data

class MainDiffCallback : DiffUtil.ItemCallback<Data>() {
    override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {

        return oldItem == newItem
    }
}