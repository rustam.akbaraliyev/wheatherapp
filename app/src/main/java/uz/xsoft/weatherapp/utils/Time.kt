package uz.xsoft.weatherapp.utils

import java.text.SimpleDateFormat
import java.util.*

object Time {
    fun getHourMinut(time: Long): String {
        val date = Date(time * 1000)
        val sdf = SimpleDateFormat("hh:mm aa").format(date)
        return sdf
    }

    fun getHour(time: Long): String {
        val date = Date(time * 1000)
        val sdf = SimpleDateFormat("hh aa").format(date)
        return sdf
    }

    fun getDate(time: Long): String {
        val date = Date(time * 1000)
        val sdf = SimpleDateFormat("dd-MM").format(date)
        return sdf
    }

}