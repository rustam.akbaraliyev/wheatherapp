package uz.xsoft.weatherapp.ui.fragments

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_main.*
import uz.xsoft.weatherapp.R
import uz.xsoft.weatherapp.extensions.startAction
import uz.xsoft.weatherapp.ui.adapters.MainListAdapter
import uz.xsoft.weatherapp.viewmodels.MainViewModel

class MainFragment : Fragment() {
    private var column = 1
    private val adapter =
        MainListAdapter {
            val action = MainFragmentDirections.actionToResult(it)
            startAction(action)
        }

    private val mainViewModel by lazy {
        ViewModelProviders.of(activity!!)[MainViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater
            .inflate(
                R.layout.fragment_main,
                container,
                false
            )


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Configuration.ORIENTATION_LANDSCAPE == resources.configuration.orientation)
            column = 2

        Glide.with(list)
            .load(R.drawable.bg_cloud)
            .into(bg)


        list.apply {
            layoutManager = GridLayoutManager(activity!!, column)
            adapter = this@MainFragment.adapter
        }

        mainViewModel.getCities().observe(viewLifecycleOwner, Observer {
            if (it[0].e != null)
                Snackbar
                    .make(view, R.string.connection, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.tagain) {
                        mainViewModel.loadCities()
                    }
                    .show()
            else {
                progress.visibility = View.GONE
                adapter.submitList(it)
            }
        })

    }
}


