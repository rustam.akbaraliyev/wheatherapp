package uz.xsoft.weatherapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_result.*
import uz.xsoft.weatherapp.R
import uz.xsoft.weatherapp.ui.adapters.ForecastAdapter
import uz.xsoft.weatherapp.utils.Time
import uz.xsoft.weatherapp.viewmodels.ResultViewModel

class ResultFragment : Fragment() {
    private val viewModel by lazy {
        ViewModelProviders.of(activity!!)[ResultViewModel::class.java]
    }
    private val datas by lazy {
        ResultFragmentArgs.fromBundle(arguments!!).model
    }
    private val adapter by lazy {
        ForecastAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel.loadForecast(datas.id!!)
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.apply {
            adapter = this@ResultFragment.adapter
            layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        }
        Glide.with(view)
            .load(R.drawable.bg_cloud)
            .into(bg)

        load()

        viewModel.getForecast(datas.id!!).observe(viewLifecycleOwner, Observer {
            adapter.submitList(it.list)
        })
    }


    private fun load() {
        sunrise.text = "Sunrise:  ${Time.getHourMinut(datas.sys!!.sunrise)}"
        sunset.text = "Sunset: ${Time.getHourMinut(datas.sys!!.sunset)}"
        seems.text = "Feels like: ${datas.wind!!.deg}"
        wind.text = "Wind: ${datas.wind!!.speed} m/s"
        chance.text = "Chance of rain: ${datas.clouds!!.all}%"
        humidity.text = "Humidity: ${datas.main!!.humidity}%"
        pressure.text = "Pressure: ${datas.main!!.pressure} hPa"
        visib.text = "Visibility: ${datas.sys!!.message}"
        min.text = "Min temp: ${datas.main!!.temp_min}"
        max.text = "Max temp: ${datas.main!!.temp_max}"
        name.text = datas.name
        main.text = datas.weather!!.get(0).description


    }


}