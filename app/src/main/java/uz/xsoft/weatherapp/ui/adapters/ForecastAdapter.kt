package uz.xsoft.weatherapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_forecast.view.*
import uz.xsoft.weatherapp.R
import uz.xsoft.weatherapp.data.models.Data
import uz.xsoft.weatherapp.utils.MainDiffCallback
import uz.xsoft.weatherapp.utils.Time

class ForecastAdapter() : ListAdapter<Data, ForecastAdapter.Holder>(MainDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_forecast, parent, false)
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: Data) {
            itemView.time.text = Time.getHour(data.dt!!)
            Glide.with(itemView)
                .load("http://openweathermap.org/img/w/${data.weather?.get(0)?.icon}.png")
                .into(itemView.icon)
            itemView.degree.text = "${data.main!!.temp}"
            itemView.date.text = Time.getDate(data.dt!!)
        }
    }
}