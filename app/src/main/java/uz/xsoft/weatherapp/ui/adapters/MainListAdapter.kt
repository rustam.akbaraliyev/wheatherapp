package uz.xsoft.weatherapp.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_main.view.*
import uz.xsoft.weatherapp.R
import uz.xsoft.weatherapp.data.models.Data
import uz.xsoft.weatherapp.utils.MainDiffCallback
import uz.xsoft.weatherapp.utils.Time

class MainListAdapter(private val clickListener: (Data) -> Unit) :
    ListAdapter<Data, MainListAdapter.Holder>(MainDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        return Holder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.item_main, parent, false)
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(city: Data, clickListener: (Data) -> Unit) {
            itemView.name.text = city.name
            itemView.temp.text = city.main?.temp.toString()
            itemView.time.text = Time.getHourMinut(city.dt!!)
            Glide.with(itemView)
                .load("http://openweathermap.org/img/w/${city.weather?.get(0)?.icon}.png")
                .into(itemView.icon)

            itemView.setOnClickListener {
                clickListener(city)
            }
        }
    }
}