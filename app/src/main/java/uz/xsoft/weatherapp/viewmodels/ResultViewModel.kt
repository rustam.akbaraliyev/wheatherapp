package uz.xsoft.weatherapp.viewmodels

import androidx.lifecycle.ViewModel
import uz.xsoft.weatherapp.data.models.Data
import uz.xsoft.weatherapp.data.repositories.ResultRepo

class ResultViewModel : ViewModel() {
    private val repo = ResultRepo()

    fun getForecast(id: Long) =
        repo.liveForecast(id)

    fun loadForecast(id: Long) = repo.loadForecast(id)


}
