package uz.xsoft.weatherapp.viewmodels

import androidx.lifecycle.ViewModel
import uz.xsoft.weatherapp.data.repositories.MainRepo

class MainViewModel : ViewModel() {
    private val mainRepo = MainRepo()

    fun getCities() = mainRepo.liveCities()

    fun loadCities() = mainRepo.loadCities()
}