package uz.xsoft.weatherapp.retrofit

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import uz.xsoft.weatherapp.data.models.Forecast
import uz.xsoft.weatherapp.data.models.Loader


interface ApiInterface {
    @GET("group?")
    fun getCountries(@Query("id") id: String): Observable<Loader>

    @GET("forecast?")
    fun getDayWeather(@Query("id") id: Long): Observable<Forecast>

}