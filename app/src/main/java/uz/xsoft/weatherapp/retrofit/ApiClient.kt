package uz.xsoft.weatherapp.retrofit

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {
    private const val BASE_URl = "http://api.openweathermap.org/data/2.5/"

    fun getRetrofit(): ApiInterface {
        val client = OkHttpClient.Builder()
            .addInterceptor(QueryInterceptor())
            .build()

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URl)
            .client(client)
            .build()
            .create(ApiInterface::class.java)
    }
}
