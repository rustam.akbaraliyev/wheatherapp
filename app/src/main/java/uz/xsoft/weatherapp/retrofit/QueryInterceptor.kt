package uz.xsoft.weatherapp.retrofit

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

class QueryInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url().newBuilder()
            .addQueryParameter("APPID", "04abbb621bd9e264eff068f8258d4180")
            .addQueryParameter("units", "metric")
            .build()

        val request = chain.request().newBuilder()
            .url(url)
            .build()
        Log.d("RRR", "$url")
        return chain.proceed(request)

    }

}