package uz.xsoft.weatherapp.data.repositories

import androidx.lifecycle.LiveData
import uz.xsoft.weatherapp.data.models.Data

interface MainRepository {
    fun liveCities(): LiveData<List<Data>>

    fun loadCities()
}