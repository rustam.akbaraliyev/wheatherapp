package uz.xsoft.weatherapp.data.repositories

import androidx.lifecycle.LiveData
import uz.xsoft.weatherapp.data.models.Forecast

interface ResultRepository {
    fun liveForecast(id: Long): LiveData<Forecast>
    fun loadForecast(id: Long)
}