package uz.xsoft.weatherapp.data.models

data class City(
    var id: Long,
    var name: String,
    var coord: Coord,
    var country: String
)

data class Forecast(
    var message: Double? = null,
    var cnt: Int? = null,
    var city: City? = null,
    var list: List<Data>? = null,
    var e: Throwable? = null
)
