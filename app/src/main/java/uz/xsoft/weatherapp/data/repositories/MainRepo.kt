package uz.xsoft.weatherapp.data.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import uz.xsoft.weatherapp.data.models.Data
import uz.xsoft.weatherapp.data.models.Loader
import uz.xsoft.weatherapp.retrofit.ApiClient

class MainRepo : MainRepository {
    private val api = ApiClient.getRetrofit()
    private val liveData: MutableLiveData<List<Data>> = MutableLiveData()
    private val citiesId = "524901,703448,2643743,1512569,3117735,5128581,2950159,4905599,6539761,6542283,745044"

    override fun liveCities(): LiveData<List<Data>> {
        if (liveData.value == null)
            loadCities()
        return liveData
    }

    override fun loadCities() {
        api.getCountries(citiesId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Loader> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable?) {
                }

                override fun onNext(value: Loader?) {
                    liveData.postValue(value?.list)
                }

                override fun onError(e: Throwable?) {
                    liveData.postValue(listOf(Data(e = e)))
                }
            })

    }

}