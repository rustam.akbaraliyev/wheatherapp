package uz.xsoft.weatherapp.data.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import uz.xsoft.weatherapp.data.models.Forecast
import uz.xsoft.weatherapp.retrofit.ApiClient

class ResultRepo : ResultRepository {
    private val api = ApiClient.getRetrofit()
    private val forecast = MutableLiveData<Forecast>()

    override fun loadForecast(id: Long) {

        api.getDayWeather(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<Forecast> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable?) {

                }

                override fun onNext(value: Forecast?) {
                    Log.d("RRR", "forecast ${value?.list?.size}")

                    forecast.postValue(value)
                }

                override fun onError(e: Throwable?) {
                    Log.d("RRR", "forecast error ${e?.message}")

                    forecast.postValue(Forecast(e = e))
                }

            })
    }

    override fun liveForecast(id: Long): LiveData<Forecast> {
        if (forecast.value == null)
            loadForecast(id)
        return forecast
    }


}