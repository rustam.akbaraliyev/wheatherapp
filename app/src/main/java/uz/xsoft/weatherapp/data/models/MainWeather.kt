package uz.xsoft.weatherapp.data.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class Weather(
    var id: Int,
    var main: String,
    var description: String,
    var icon: String
): Serializable

data class Wind(
    var speed: Double,
    var deg: Double
): Serializable

data class Snowing(
    @SerializedName("1h")
    var hour: Float,
    @SerializedName("3h")
    var threehour: Float
): Serializable

data class Clouds(
    var all: Int
): Serializable

data class Coord(
    var lon: Double,
    var lat: Double
): Serializable

data class Sys(
    var type: Int,
    var id: Long,
    var message: Double,
    var country: String,
    var sunrise: Long,
    var sunset: Long

): Serializable

data class Main(
    var temp: Double,
    var pressure: Float,
    var humidity: Int,
    var temp_min: Double,
    var temp_max: Double,
    var temp_kf: Double,
    var sea_level: Double,
    var grnd_level: Double
): Serializable

data class Data(
    var coord: Coord? = null,
    var weather: List<Weather>? = null,
    var base: String? = null,
    var main: Main? = null,
    var wind: Wind? = null,
    var clouds: Clouds? = null,
    var rain: Snowing? = null,
    var snow: Snowing? = null,
    var dt: Long? = null,
    var sys: Sys? = null,
    var id: Long? = null,
    var name: String? = null,
    var cod: Long? = null,
    var e: Throwable? = null,
    var dt_txt: String? = null
) : Serializable

data class Loader(
    var cnt: Int,
    var list: List<Data>
)